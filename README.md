# Ssl Certificate App

First, you need those few things: 

	A server running on a linux distribution with root access (via SSH) 

	NodeJS: https://nodejs.org/en/ 

	Express : npm install express 

	Certbot 

To install certbot, copy-paste those lines in a terminal : 

	$ sudo add-apt-repository ppa:certbot/certbot 

	$ sudo apt-get update 

	$ sudo apt-get install certbot 
		
Second, you will generate an SSL certificate with certbot : 

	$ certbot certonly --manual 



Now, don’t continue. You need to run a web server with Node & Express. 

	Create a directory with the name you want, e.g : server 

	In this directory, create a JS file which will run your server. Keep it empty for the moment as I’ll provide you with a ready-to-copy/paste source code. 

	In this directory, create two directories : .well-known , and inside this one, create : acme-challenge . 

	In the directory : acme-challenge place the file you created before : a-string 


Link - https://itnext.io/node-express-letsencrypt-generate-a-free-ssl-certificate-and-run-an-https-server-in-5-minutes-a730fbe528ca 
